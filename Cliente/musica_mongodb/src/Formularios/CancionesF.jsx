import React, { useState } from "react";
import Boton from "../Botones/boton";
import InputCan from "../Inputs/InputCan";
import Axios from "axios";

function CancionesF({ accionBoton, volver , artistaParaCambio, albumParaCambio, indicarCambio}) {
  const [limpiar, setLimpiar] = useState(null);
  const [valido, setValido] = useState(false);

  const [nombreArtista, setNombreArtista] = useState("");
  const [nombreAlbum, setNombreAlbum] = useState("");
  const [cancion, setCancion] = useState("");

  const registrar = () => {
    if (valido) {
      setLimpiar("");
      Axios.post("http://localhost:3000/registra_canciones", {
        nombreArtista,
        nombreAlbum,
        cancion,
      })
        .then(() => {
          setLimpiar(null);
          artistaParaCambio(nombreArtista);
          albumParaCambio(nombreAlbum);
          indicarCambio(true);
          setNombreArtista("");
          setNombreAlbum("");
        })
        .catch();
    }
  };

  return (
    <>
      <div className="inputs">
        <div className="input-caja">
          <InputCan
            capturaDato={setNombreArtista}
            nombreArtista={nombreArtista}
            nombreAlbum={nombreAlbum}
            cancion={cancion}
            place="Nombre de artista"
            validar={setValido}
          />
        </div>{" "}
        <div className="input-caja">
          <InputCan
            capturaDato={setNombreAlbum}
            nombreArtista={nombreArtista}
            nombreAlbum={nombreAlbum}
            cancion={cancion}
            place="Nombre de album"
            validar={setValido}
          />
        </div>
        <div className="input-caja">
          <InputCan
            capturaDato={setCancion}
            nombreArtista={nombreArtista}
            nombreAlbum={nombreAlbum}
            cancion={cancion}
            place="Canción"
            validar={setValido}
            limpiar={limpiar}
          />
        </div>
      </div>
      <div className="input-caja boton">
        <input
          type="Submit"
          value="Registrar canciones"
          className="input"
          required
          onClick={(e) => {
            e.preventDefault();
            registrar(e);
          }}
        />
      </div>

      <Boton accionBoton={accionBoton} nombre="Cerrar" estado={false} />
      <Boton accionBoton={volver} nombre="Volver" estado={false} />
    </>
  );
}

export default CancionesF;
