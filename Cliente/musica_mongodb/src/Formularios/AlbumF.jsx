import React, { useState } from "react";
import Boton from "../Botones/boton";
import InputAlbum from "../Inputs/InputAlbum";
import CancionesF from "./CancionesF";
import Axios from "axios";

function AlbumF({ accionBoton, indicarCambio, artistaParaCambio, albumParaCambio }) {
  const [album, setAlbum] = useState("");
  const [limpiar, setLimpiar] = useState(null);
  const [valido, setValido] = useState(false);

  const [albumForm, setAlbumForm] = useState(false);

  const [nombreArtista, setNombreArtista] = useState("");
  const [nombreAlbum, setNombreAlbum] = useState("");
  const [fecha, setFecha] = useState("");
  const [genero, setGenero] = useState("");
  const [duracion, setDuracion] = useState("");

  const registrar = (e) => {
    if (valido) {
      setLimpiar("");
      Axios.post("http://localhost:3000/registra_datosAlbum", {
        nombreArtista,
        nombreAlbum,
        fecha,
        genero,
        duracion,
      })
        .then(() => {
          setLimpiar(null);
          artistaParaCambio(nombreArtista);
          albumParaCambio(nombreAlbum);
          indicarCambio(true);
          setNombreArtista("");
          setNombreAlbum("");
          setFecha("");
          setGenero("");
          setDuracion("");
          console.log("Hubo respuesta");
        })
        .catch(() => {
          console.log("Hubo un error");
        });
    }
  };

  return (
    <>
      {album ? (
        <CancionesF accionBoton={setAlbum}  volver={accionBoton} artistaParaCambio={artistaParaCambio} albumParaCambio={albumParaCambio} indicarCambio={indicarCambio}/>
      ) : (
        <>
          {albumForm ? (
            <>
              <div className="inputs">
                <div className="input-caja">
                  <InputAlbum
                    capturaDato={setNombreArtista}
                    nombreArtista={nombreArtista}
                    nombreAlbum={nombreAlbum}
                    fecha={fecha}
                    genero={genero}
                    duracion={duracion}
                    place="Nombre de artista"
                    validar={setValido}
                    limpiar={limpiar}
                  />
                </div>{" "}
                <div className="input-caja">
                  <InputAlbum
                    capturaDato={setNombreAlbum}
                    nombreArtista={nombreArtista}
                    nombreAlbum={nombreAlbum}
                    fecha={fecha}
                    genero={genero}
                    duracion={duracion}
                    place="Nombre de album"
                    validar={setValido}
                    limpiar={limpiar}
                  />
                </div>{" "}
                <div className="input-caja">
                  <InputAlbum
                    capturaDato={setFecha}
                    nombreArtista={nombreArtista}
                    nombreAlbum={nombreAlbum}
                    fecha={fecha}
                    genero={genero}
                    duracion={duracion}
                    place="Año"
                    validar={setValido}
                    limpiar={limpiar}
                  />
                </div>
                <div className="input-caja">
                  <InputAlbum
                    capturaDato={setGenero}
                    nombreArtista={nombreArtista}
                    nombreAlbum={nombreAlbum}
                    fecha={fecha}
                    genero={genero}
                    duracion={duracion}
                    place="Genero"
                    validar={setValido}
                    limpiar={limpiar}
                  />
                </div>
                <div className="input-caja">
                  <InputAlbum
                    capturaDato={setDuracion}
                    nombreArtista={nombreArtista}
                    nombreAlbum={nombreAlbum}
                    fecha={fecha}
                    genero={genero}
                    duracion={duracion}
                    place="Duración"
                    validar={setValido}
                    limpiar={limpiar}
                  />
                </div>
              </div>

              <div className="input-caja boton">
                <input
                  type="Submit"
                  value="Registrar album"
                  className="input"
                  required
                  onClick={(e) => {
                    e.preventDefault();
                    registrar(e);
                  }}
                />
              </div>

              <Boton
                accionBoton={setAlbumForm}
                nombre="Cerrar"
                estado={false}
              />
              {/* 
              <Boton
                accionBoton={setAlbum}
                nombre="Registrar canciones"
                estado={true}
              /> */}
              <Boton accionBoton={accionBoton} nombre="Volver" estado={false} />
            </>
          ) : (
            <>
              {" "}
              <Boton
                accionBoton={setAlbumForm}
                nombre="Registrar album"
                estado={true}
              />
              <Boton
                accionBoton={setAlbum}
                nombre="Registrar canciones"
                estado={true}
              />
              <Boton accionBoton={accionBoton} nombre="Volver" estado={false} />
            </>
          )}
        </>
      )}
    </>
  );
}

export default AlbumF;
