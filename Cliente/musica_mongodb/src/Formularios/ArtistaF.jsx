import React, { useState } from "react";
import Boton from "../Botones/boton";
import InputArtista from "../Inputs/InputArtista";
import Axios from "axios";

function ArtistaF({ accionBoton, indicarCambio }) {
  const [nombreArtista, setNombreArtista] = useState("");
  const [limpiar, setLimpiar] = useState(null);
  const [valido, setValido] = useState(false);

  const registrar = () => {
    if (valido) {
      setLimpiar("");
      Axios.post("http://localhost:3000/registra_artista", {
        nombreArtista,
      })
        .then(() => {
          indicarCambio(true);
          setLimpiar(null);
          setNombreArtista("");
          console.log("Hubo respuesta");
        })
        .catch(() => {
          console.log("Hubo un error");
        });
    }
  };

  return (
    <>
      <div className="inputs">
        <div className="input-caja">
          <InputArtista
            capturaNombre={setNombreArtista}
            nombreArtista={nombreArtista}
            place="Nombre de artista"
            validar={setValido}
            limpiar={limpiar}
          />
        </div>
      </div>
      <div className="input-caja boton">
        <input
          type="Submit"
          value="Registrar artista"
          className="input"
          required
          onClick={(e) => {
            e.preventDefault();
            registrar(e);
          }}
        />
      </div>
      <Boton accionBoton={accionBoton} nombre="Cerrar" estado={false} />
    </>
  );
}

export default ArtistaF;
