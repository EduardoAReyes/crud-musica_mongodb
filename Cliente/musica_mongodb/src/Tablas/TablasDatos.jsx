import React, { useState, useEffect } from "react";
import Axios from "axios";
import InputB from "../Inputs/InputB";
import ActuInput from "../Actualizacion/ActuInput";

function TablasDatos({
  setUltimoCampo,
  ultimoCampo,
  indicarCambio,
  huboCambio,
}) {
  const [nombreArtista, setNombreArtista] = useState("");
  const [nombreAlbum, setNombreAlbum] = useState("");
  const [validoBArt, setValidoBArt] = useState(false);
  const [validoBAlb, setValidoBAlb] = useState(false);
  const [limpiar, setLimpiar] = useState(null);

  const [Actualizacion, setActualizacion] = useState(false);

  // const [ActualizarA, setActualizarA] = useState("");
  const [place, setPlace] = useState("");
  const [nombre, setNombre] = useState("");

  const [cambioActualizacion, setCambioActualizacion] = useState(false);

  // useEffect(() => {
  //   Axios.post("http://localhost:3000/consultaDatos_album", {
  //     nombreArtista:,
  //     nombreAlbum: ,
  //   }).then(() => {});
  //   setCambioActualizacion(false);
  // }, [cambioActualizacion]);

  const buscarDatosAlbum = (nombreArtista, nombreAlbum) => {
    setLimpiar("");
    if (validoBArt && validoBAlb) {
      Axios.post("http://localhost:3000/consultaDatos_album", {
        nombreArtista: nombreArtista,
        nombreAlbum: nombreAlbum,
      }).then((res) => {
        setLimpiar(null);
        setUltimoCampo(res.data);
        console.log(ultimoCampo);
      });
    }
  };
  return (
    <>
      <InputB
        setDatoBuscar={setNombreArtista}
        datoBuscar={nombreArtista}
        setValidoB={setValidoBArt}
        place="Nombre de artista"
        limpiar={limpiar}
      />
      <InputB
        setDatoBuscar={setNombreAlbum}
        datoBuscar={nombreAlbum}
        setValidoB={setValidoBAlb}
        place="Nombre de album"
        limpiar={limpiar}
      />
      <button
        className="boton-buscar"
        onClick={() => {
          buscarDatosAlbum(nombreArtista, nombreAlbum);
        }}
      >
        Buscar
      </button>
      {Actualizacion && (
        <ActuInput
          indicarArtista={nombreArtista}
          indicarNombre={nombre}
          indicarPlace={place}
          cerrar={setActualizacion}
          indicarCambio={setCambioActualizacion}
          setUltimoCampo={setUltimoCampo}
          ultimoCampo={ultimoCampo}
          // indicarCampo={campoActualizar}
        />
      )}
      <div className="centrar">
        <form>
          <div className="tabla-contenedor">
            <table className="tabla">
              <thead>
                <tr>
                  <th scope="col">Datos de album</th>
                  <th scope="col"></th>
                  <th scope="col"></th>
                </tr>
              </thead>
              {/* style={{widt:"150px"}} */}
              <tbody>
                {Object.entries(ultimoCampo)
                  .filter(([clave]) => clave !== "Canciones")
                  .map(([clave, valor]) => (
                    <tr>
                      <th style={{ height: "50px" }}>{clave}:</th>
                      <td>{valor}</td>
                      <td>
                        <button
                          className="boton-buscar"
                          onClick={(e) => {
                            e.preventDefault();
                            // setActualizarA();
                            setNombre(clave);
                            setPlace(valor);
                            // setCampoActualizar(valor);
                            setActualizacion(true);
                          }}
                        >
                          Actualizar
                        </button>
                      </td>
                    </tr>
                  ))}
              </tbody>
            </table>
          </div>
        </form>
      </div>
    </>
  );
}

export default TablasDatos;
