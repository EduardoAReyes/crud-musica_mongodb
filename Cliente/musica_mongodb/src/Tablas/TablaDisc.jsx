import React, { useEffect, useState } from "react";
import Axios from "axios";
import InputB from "../Inputs/InputB";

function TablaDisc({ setUltimoCampo, ultimoCampo, indicarCambio, huboCambio, nombreArtistaCambio, nombreAlbumCambio }) {
  // Primero con solo artistas

  const [nombreArtista, setNombreArtista] = useState("");
  const [nombreAlbum, setNombreAlbum] = useState("");
  const [cancionEliminar, setCancionEliminar] = useState("");

  const [validoBArt, setValidoBArt] = useState(false);
  const [validoBAlb, setValidoBAlb] = useState(false);
  const [limpiar, setLimpiar] = useState(null);

  // let nombreACam = nombreArtistaCambio;
  // let nombreAlbumCam = nombreAlbumCambio;
  // useEffect(() => {
  //   Axios.post("http://localhost:3000/Consulta_Canciones",{
  //     nombreArtista: nombreACam,
  //     nombreAlbum: nombreAlbumCam
  //   }).then((res)=>{
  //     setUltimoCampo(res.data);
  //   });
  //   indicarCambio(true);
  // }, [huboCambio])
  

  const buscarCanciones = (nombreArtista, nombreAlbum) => {
    setLimpiar("");
    if (validoBArt && validoBAlb) {
      Axios.post("http://localhost:3000/Consulta_Canciones", {
        nombreArtista: nombreArtista,
        nombreAlbum: nombreAlbum,
      }).then((res) => {
        setLimpiar(null);
        setUltimoCampo(res.data);
        console.log("Log en canciones", ultimoCampo);
      });
    }
  };

  const eliminarCancion = (cancion, nombreArtista, nombreAlbum) =>{
    Axios.post("http://localhost:3000/EliminaCancion",{
      nombreArtista: nombreArtista,
      nombreAlbum: nombreAlbum,
      cancionEliminar: cancion,
    }).then((res)=>{
      setUltimoCampo(res.data);
    });
  }

  const cancionesMapeadas = ultimoCampo.map((elemt) => elemt.Canciones);

  console.log(cancionesMapeadas);

  return (
    <>
      <InputB
        setDatoBuscar={setNombreArtista}
        datoBuscar={nombreArtista}
        setValidoB={setValidoBArt}
        place="Nombre de artista"
        limpiar={limpiar}
      />
      <InputB
        setDatoBuscar={setNombreAlbum}
        datoBuscar={nombreAlbum}
        setValidoB={setValidoBAlb}
        place="Nombre de album"
        limpiar={limpiar}
      />
      <button
        className="boton-buscar"
        onClick={() => {
          buscarCanciones(nombreArtista, nombreAlbum);
        }}
      >
        Buscar
      </button>
      <div className="centrar">
        <form>
          <div className="tabla-contenedor">
            <table className="tabla">
              <thead>
                <tr>
                  <th scope="col">Lista de canciones</th>
                  <th scope="col"></th>
                </tr>
              </thead>
              <tbody>
                {/* {Object.entries(ultimoCampo).map(([clave,valor]) => (
                  <tr key={clave}>
                    <td>{valor.Canciones[clave]}</td>
                    <td>
                      <button
                        className="boton-buscarF"
                        onClick={(e) => {
                          e.preventDefault();
                        }}
                      >
                        Eliminar
                      </button>
                    </td>
                  </tr>
                ))} */}
                {cancionesMapeadas.map((canciones, index) => (
                  <ul key={index}>
                    {canciones.map((cancion, i) => (
                      <li key={i} style={{ padding: "20px" }}>
                        {cancion}{" "}
                        <button
                          className="boton-buscarF"
                          style={{ margin: "20px" }}
                          onClick={(e) => {
                            e.preventDefault();
                            eliminarCancion(cancion,nombreArtista,nombreAlbum);
                          }}
                        >
                          Eliminar
                        </button>
                      </li>
                    ))}
                  </ul>
                ))}
              </tbody>
            </table>
          </div>
        </form>
      </div>
    </>
  );
}

export default TablaDisc;
