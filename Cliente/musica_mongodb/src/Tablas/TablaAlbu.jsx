import React, { useEffect, useState } from "react";
import Axios from "axios";
import InputB from "../Inputs/InputB";

function TablaAlbu({
  setUltimoCampo,
  ultimoCampo,
  indicarCambio,
  huboCambio,
  nombreArtistaCambio,
  nombreAlbumCambio,
}) {
  const [datoBuscar, setDatoBuscar] = useState("");
  const [validoB, setValidoB] = useState(false);
  const [limpiar, setLimpiar] = useState(null);

  let nombreA = nombreArtistaCambio;
  let nombreAlbum = nombreAlbumCambio;

  console.log(nombreA);
  console.log(nombreAlbum);
  useEffect(() => {
    Axios.post("http://localhost:3000/busca_album", {
      nombreArtista: nombreA,
      nombreAlbum: nombreAlbum,
    }).then((res) => {
      setUltimoCampo(res.data);
    });
    indicarCambio(false);
  }, [huboCambio]);

  const buscarAlbum = (nombreArtista) => {
    setLimpiar("");
    if (validoB) {
      Axios.post("http://localhost:3000/busca_album", {
        nombreArtista: nombreArtista,
      }).then((res) => {
        setLimpiar(null);
        setUltimoCampo(res.data);
        console.log('Log en album');
        console.log(ultimoCampo);
      });
    }
  };

  const eliminarAlbum = (nombreAlbum, nombreArtista) => {
    Axios.post("http://localhost:3000/EliminaAlbum", {
      nombreArtista: nombreArtista,
      nombreAlbum: nombreAlbum,
    }).then((res) => {
      // indicarCambio(true);
      setUltimoCampo(res.data);
    });
  };

  return (
    <>
      <InputB
        setDatoBuscar={setDatoBuscar}
        datoBuscar={datoBuscar}
        setValidoB={setValidoB}
        place="Nombre de artista"
        limpiar={limpiar}
      />
      <button
        className="boton-buscar"
        onClick={() => {
          buscarAlbum(datoBuscar);
        }}
      >
        Buscar
      </button>
      <div className="centrar">
        <form>
          <div className="tabla-contenedor">
            <table className="tabla">
              <thead>
                <tr>
                  <th scope="col">Lista de albumes</th>
                </tr>
              </thead>
              <tbody>
                {ultimoCampo.map((val) => (
                  <tr>
                    <td>{val}</td>
                    <td>
                      <button
                        className="boton-buscarF"
                        onClick={(e) => {
                          e.preventDefault();
                          eliminarAlbum(val, datoBuscar);
                          // buscarAlbum(val);
                        }}
                      >
                        Eliminar
                      </button>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </form>
      </div>
    </>
  );
}

export default TablaAlbu;
