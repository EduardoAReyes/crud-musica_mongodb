import React, { useEffect } from "react";
import Axios from "axios";

function TablaArt({ setUltimoCampo, ultimoCampo, indicarCambio, huboCambio }) {
  useEffect(() => {
    Axios.get("http://localhost:3000/busca_artista").then((res) => {
      setUltimoCampo(res.data);
    });
  }, []);

  useEffect(() => {
    Axios.get("http://localhost:3000/busca_artista").then((res) => {
      setUltimoCampo(res.data);
    });
    indicarCambio(false);
  }, [huboCambio]);

  const eliminarNombre = (nombre) => {
    Axios.post("http://localhost:3000/EliminaArtista", {
      nombreArtista: nombre,
    }).then(() => {
      indicarCambio(true);
    });
  };

  return (
    <>
      <div className="centrar">
        <form>
          <div className="tabla-contenedor">
            <table className="tabla">
              <thead>
                <tr>
                  <th scope="col">Lista de artistas</th>
                  <th scope="col"></th>
                </tr>
              </thead>
              <tbody>
                {ultimoCampo.map((val) => (
                  <tr key={val}>
                    <td>{val}</td>
                    <td>
                      <button
                        className="boton-buscarF"
                        onClick={(e) => {
                          e.preventDefault();
                          eliminarNombre(val);
                        }}
                      >
                        Eliminar
                      </button>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </form>
      </div>
    </>
  );
}

export default TablaArt;
