import React, { useState } from "react";
import LMenu from "../Componentes principales/LMenu.jsx";
import Boton from "../Botones/boton.jsx";
import ArtistaF from "../Formularios/ArtistaF.jsx";
import AlbumF from "../Formularios/AlbumF.jsx";
import TablaArt from "../Tablas/TablaArt.jsx";
import TablaAlbu from "../Tablas/TablaAlbu.jsx";
import TablasDatos from "../Tablas/TablasDatos.jsx";
import TablaDisc from "../Tablas/TablaDisc.jsx";
import Axios from "axios";

function Discos() {
  const [habilitarArtista, setHabilitarArtista] = useState(false);
  const [habilitarAlbum, setHabilitarAlbum] = useState(false);

  const [tablaA, setTablaA] = useState(false);
  const [tablaAlb, setTablaAlb] = useState(false);
  const [tablaDat, setTablaDat] = useState(false);
  const [tablaDisc, setTablaDisc] = useState(false);

  const [ultimoCampo, setUltimoCampo] = useState([]);
  const [ultimoCampo2, setUltimoCampo2] = useState([]);
  const [ultimoCampoDatosA, setUltimoCampoDatosA] = useState([]);
  const [ultimoCampoCan, setUltimoCampoCan] = useState([]);

  const [cambio, setCambio] = useState(false);

  const [artistaCambio, setArtistaCambio] = useState("");
  const [albumCambio, setAlbumCambio] = useState("");

  const consultarDatosAlbums = () => {
    Axios.post("http://localhost:3000/consultaDatos_album").then(() => {});
  };

  return (
    <>
      <LMenu />
      <section className="home-section">
        <div className="text">
          <div className="formulario">
            <h2>Discos</h2>
            <form className="form">
              {habilitarArtista ? (
                <ArtistaF
                  accionBoton={setHabilitarArtista}
                  indicarCambio={setCambio}
                />
              ) : habilitarAlbum ? (
                <AlbumF
                  accionBoton={setHabilitarAlbum}
                  indicarCambio={setCambio}
                  artistaParaCambio={setArtistaCambio}
                  albumParaCambio={setAlbumCambio}
                />
              ) : (
                <>
                  <Boton
                    accionBoton={setHabilitarArtista}
                    nombre="Agregar nuevo artista"
                    estado={true}
                  />
                  <Boton
                    accionBoton={setHabilitarAlbum}
                    nombre="Administrar albumes"
                    estado={true}
                  />
                </>
              )}
            </form>
          </div>
        </div>
        <div className="centrar">
          <div>
            <button
              className="boton-buscarF"
              onClick={() => {
                setUltimoCampo2([]);
                setUltimoCampoDatosA([]);
                setUltimoCampoCan([]);
                setTablaA(true);
                setTablaAlb(false);
                setTablaDat(false);
                setTablaDisc(false);
              }}
            >
              Artistas
            </button>
            <button
              className="boton-buscarF"
              onClick={() => {
                setUltimoCampo([]);
                setUltimoCampoDatosA([]);
                setUltimoCampoCan([]);
                setTablaAlb(true);
                setTablaA(false);
                setTablaDat(false);
                setTablaDisc(false);
              }}
            >
              Albums
            </button>
            <button
              className="boton-buscarF"
              onClick={() => {
                setUltimoCampo([]);
                setUltimoCampo2([]);
                setUltimoCampoCan([]);
                setTablaDat(true);
                setTablaA(false);
                setTablaAlb(false);
                setTablaDisc(false);
                // consultarDatosAlbums();
              }}
            >
              Datos Albums
            </button>
            <button
              className="boton-buscarF"
              onClick={() => {
                setUltimoCampo([]);
                setUltimoCampo2([]);
                setUltimoCampoDatosA([]);
                setTablaDisc(true);
                setTablaA(false);
                setTablaAlb(false);
                setTablaDat(false);
              }}
            >
              Canciones
            </button>
            <button
              nombre="R"
              className="boton-bregreF"
              onClick={() => {
                setUltimoCampo([]);
                setUltimoCampo2([]);
                setUltimoCampoDatosA([]);
                setUltimoCampoCan([]);
                setTablaA(false);
                setTablaAlb(false);
                setTablaDat(false);
                setTablaDisc(false);
              }}
            >
              R
            </button>
          </div>
        </div>
        {tablaA && (
          <TablaArt
            setUltimoCampo={setUltimoCampo}
            ultimoCampo={ultimoCampo}
            indicarCambio={setCambio}
            huboCambio={cambio}
          />
        )}
        {tablaAlb && (
          <TablaAlbu
            setUltimoCampo={setUltimoCampo2}
            ultimoCampo={ultimoCampo2}
            indicarCambio={setCambio}
            huboCambio={cambio}
            nombreArtistaCambio={artistaCambio}
            nombreAlbumCambio={albumCambio}
          />
        )}
        {tablaDat && (
          <TablasDatos
            setUltimoCampo={setUltimoCampoDatosA}
            ultimoCampo={ultimoCampoDatosA}
            indicarCambio={setCambio}
            huboCambio={cambio}
          />
        )}
        {tablaDisc && (
          <TablaDisc
            setUltimoCampo={setUltimoCampoCan}
            ultimoCampo={ultimoCampoCan}
            indicarCambio={setCambio}
            huboCambio={cambio}
            nombreArtistaCambio={artistaCambio}
            nombreAlbumCambio={albumCambio}
          />
        )}
      </section>
    </>
  );
}

export default Discos;
