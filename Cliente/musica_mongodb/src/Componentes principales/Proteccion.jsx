import React from "react";
import { Outlet, Navigate } from "react-router-dom";

function Proteccion({ hijo }) {
  let auth = localStorage.getItem("token");
  return auth ? <Outlet /> : <Navigate to="/" />;
}

export default Proteccion;
