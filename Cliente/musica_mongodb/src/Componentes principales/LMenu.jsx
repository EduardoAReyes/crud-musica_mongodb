import React from "react";
import { Link } from "react-router-dom";

function LMenu() {
  return (
    <div className="sidebar">
      <div className="logo_content">
        <div className="logo">
          <i className="bx bxl-c-plus-plus"></i>
          <div className="logo_name">NahumZone</div>
        </div>
      </div>
      <nav>
        <ul className="nav_list">
          <li>
            <Link to="/Menu">
              <i class="bx bx-disc"></i>
              <span className="links_name">Discos</span>
            </Link>
          </li>
          <li>
            <Link to="/">
              <i className="bx bx-arrow-from-right"></i>
              <span className="links_name">Cerrar sesion</span>
            </Link>
          </li>
        </ul>
      </nav>
    </div>
  );
}

export default LMenu;
