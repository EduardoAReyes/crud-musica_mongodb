import React, { useState } from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Login from "../Login/Login";
import Proteccion from "./Proteccion";
import Discos from "../Discos/Discos";

function App() {
  const [logueado, setLogueado] = useState(false);

  const estado = (validado) => {
    setLogueado(validado);
  };

  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Login estado={estado} />} />
          <Route element={<Proteccion hijo={logueado} />}>
            <Route path="/Menu" element={<Discos />} />
          </Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
