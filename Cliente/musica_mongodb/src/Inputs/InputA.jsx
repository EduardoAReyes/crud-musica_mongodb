import React, { useState, useEffect } from "react";

function InputA({ setCampo, place, nombre }) {
  const [valorI, setValorI] = useState(place);
  useEffect(() => {
    setCampo(valorI);
  }, []);

  return (
    <>
      <input
      style={{textAlign:"center"}}
        type="text"
        className="input-buscar"
        placeholder={place}
        onChange={(e) => {
          let dato = e.target.value;
          if (dato !== "") {
            setCampo(dato);
          } else {
            setCampo(valorI);
          }
        }}
      />
    </>
  );
}

export default InputA;
