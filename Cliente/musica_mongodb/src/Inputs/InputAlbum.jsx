import React,{useEffect} from "react";

function InputAlbum({
  capturaDato,
  nombreArtista,
  nombreAlbum,
  fecha,
  genero,
  duracion,
  place,
  validar,
  limpiar,
}) {
  useEffect(() => {
    validado();
  }, [nombreArtista, nombreAlbum, fecha, genero, duracion])
  
  const validado = () => {
    if (
      nombreArtista !== "" &&
      nombreAlbum !== "" &&
      fecha !== "" &&
      genero !== "" &&
      duracion !== ""
    ) {
      validar(true);
    } else {
      validar(false);
    }
  };
  return (
    <input
      value={limpiar}
      type="text"
      className="input"
      required
      placeholder={place}
      onChange={(e) => {
        capturaDato(e.target.value);
      }}
    />
  );
}

export default InputAlbum;
