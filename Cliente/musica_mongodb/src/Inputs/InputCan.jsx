import React, { useEffect } from "react";

function InputCan({
  capturaDato,
  nombreArtista,
  nombreAlbum,
  cancion,
  place,
  validar,
  limpiar,
}) {
  useEffect(() => {
    validado();
  }, [nombreArtista, nombreAlbum, cancion]);

  const validado = () => {
    if (nombreArtista !== "" && nombreAlbum !== "" && cancion !== "") {
      validar(true);
    } else {
      validar(false);
    }
  };

  return (
    <input
      value={limpiar}
      type="text"
      className="input"
      required
      placeholder={place}
      onChange={(e) => {
        capturaDato(e.target.value);
      }}
    />
  );
}

export default InputCan;
