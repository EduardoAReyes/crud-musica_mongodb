import React, { useEffect } from "react";

function InputB({ setDatoBuscar, datoBuscar, setValidoB, place, limpiar }) {
  useEffect(() => {
    valido();
  }, [datoBuscar]);

  const valido = () => {
    if (datoBuscar !== "") {
      setValidoB(true);
    } else {
      setValidoB(false);
    }
  };

  return (
    <>
      <input
        value={limpiar}
        required
        type="text"
        className="input-buscar"
        placeholder={place}
        onChange={(e) => {
          setDatoBuscar(e.target.value);
        }}
      />
    </>
  );
}

export default InputB;
