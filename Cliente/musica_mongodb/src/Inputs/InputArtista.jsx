import React, { useEffect } from "react";

function InputArtista({
  capturaNombre,
  nombreArtista,
  place,
  validar,
  limpiar,
}) {
  useEffect(() => {
    validado();
  }, [nombreArtista]);
 
  const validado = () => {
    if (nombreArtista !== "") {
      validar(true);
    } else {
      validar(false);
    }
  };

  return (
    <>
      <input
        value={limpiar}
        type="text"
        className="input"
        required
        placeholder={place}
        onChange={(e) => {
          capturaNombre(e.target.value);
        }}
      />
    </>
  );
}

export default InputArtista;
