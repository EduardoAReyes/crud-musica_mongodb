import React from "react";

function Boton({ accionBoton, nombre, estado }) {
  return (
    <div className="input-caja boton">
      <input
        type="Submit"
        value={nombre}
        className="input"
        required
        onClick={(e) => {
          e.preventDefault();
          accionBoton(estado);
        }}
      />
    </div>
  );
}

export default Boton;
