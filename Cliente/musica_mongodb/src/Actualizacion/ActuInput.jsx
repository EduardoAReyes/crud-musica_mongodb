import React, { useState } from "react";
import InputA from "../Inputs/InputA";
import Axios from "axios";

function ActuInput({ setUltimoCampo, ultimoCampo,indicarArtista, indicarNombre, indicarPlace, cerrar, indicarCcambio }) {
  const [datoActualizar, setDatoActualizar] = useState("");

  const actualizar = () => {
    Axios.post("http://localhost:3000/actualizaAlbum", {
      nombreArtista: indicarArtista,
      caso: indicarNombre,
      datoAntiguo: indicarPlace,
      nuevoDato: datoActualizar,
    }).then((res) => {
      setUltimoCampo(res.data);
      console.log(res.data);
      indicarCcambio(true);
      cerrar(false);
    });
  };

  return (
    <>
      {" "}
      <h1 className="modal-error-existencias">
        Actualizar
        <InputA
          setCampo={setDatoActualizar}
          place={indicarPlace}
          nombre={indicarNombre}
        />
        <div className="botones-error-existencia">
          <button
            className="boton-error-existencia"
            onClick={() => {
              actualizar();
            }}
          >
            Actualizar
          </button>{" "}
          <button
            className="boton-error-existencia"
            onClick={() => {
              cerrar(false);
            }}
          >
            Cancelar
          </button>
        </div>
      </h1>
    </>
  );
}

export default ActuInput;
