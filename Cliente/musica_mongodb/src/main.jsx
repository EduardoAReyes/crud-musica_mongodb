/**
 * ? Comandos utilizados
 * * npm install react-router-dom
 * * npm install Axios
 *
 * !Link: https://www.mongodb.com/docs/drivers/node/current/quick-start/download-and-install/
 */

import React from "react";
import ReactDOM from "react-dom/client";
import App from "./Componentes principales/App.jsx";
import "./Componentes principales/index.css";
import "./Componentes principales/App.css";

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);
