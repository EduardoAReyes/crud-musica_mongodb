import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import InputLogin from "./InputLogin";
import "./Login.css";
import Axios from "axios";

function Login({ estado }) {
  const [loginNOTOK, setLoginNOTOK] = useState(false);

  const [nombre, setNombre] = useState("");
  const [contraseña, setContraseña] = useState("");
  const [autentificado, setAutentificado] = useState(false);
  const [valido, setValido] = useState(false);

  const navegar = useNavigate();

  useEffect(() => {
    window.addEventListener("beforeunload", antesDeCargar);

    return () => {
      window.removeEventListener("beforeunload", antesDeCargar);
    };
  }, []);

  const antesDeCargar = () => {
    localStorage.clear();
  };

  const conexion = () => {
    console.log("Valido en conexion: " + valido);
    if (valido) {
      setAutentificado(true);
      Axios.post("http://localhost:3000/conexion", {
        nombre,
        contraseña,
      })
        .then(() => {
          localStorage.setItem("token", autentificado);
          estado(autentificado);
          navegar(`/Menu`);
        })
        .catch(() => {
          setNombre("");
          setContraseña("");
          setLoginNOTOK(true);
          cerrarAviso();
        });
    }
  };

  let contador = 2;
  const cerrarAviso = () => {
    const intervalo = setInterval(() => {
      if (contador === 0) {
        setLoginNOTOK(false);
        clearInterval(intervalo);
      } else {
        contador = contador - 1;
        cerrarAviso();
      }
    }, 1000);
  };

  return (
    <div className="body">
      {loginNOTOK && (
        <h1 className="modal-error-login">
          El usuario o Contraseña estan incorrectos
        </h1>
      )}
      <div className="wrapper">
        <h2>Iniciar sesion</h2>
        <form>
          <InputLogin
            place="Usuario"
            setCampo={setNombre}
            campo={nombre}
            setValido={setValido}
            type="Usuario"
          />

          <InputLogin
            place="Contraseña"
            setCampo={setContraseña}
            campo={contraseña}
            setValido={setValido}
            type="password"
          />

          <div className="input-box button">
            <input
              type="Submit"
              defaultValue="Ingresar"
              onClick={(e) => {
                e.preventDefault();
                conexion();
              }}
            />
          </div>
        </form>
      </div>
    </div>
  );
}

export default Login;
/**
 * collecion
 * documentos
 * campos
 */
