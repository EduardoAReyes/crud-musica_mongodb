import React, { useEffect } from "react";

function InputLogin({ place, setCampo, campo, type, setValido}) {
  
  useEffect(() => {
    valido();
  }, [campo]);

  const valido = () => {
    if(campo!==""){
      setValido(true);
    }else{
      setValido(false);
    }
  };

  return (
    <div className="input-box">
      <input
        value={campo}
        type={type}
        placeholder={place}
        required
        onChange={(e) => {
          setCampo(e.target.value);
        }}
      />
    </div>
  );
}

export default InputLogin;
