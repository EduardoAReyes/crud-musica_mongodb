/**
 * ? dependencias utilizadas
 * * npm install express
 * * npm install morgan
 * * npm install cors
 */

import express from "express";
import morgan from "morgan";
import cors from "cors";
const app = express();

import rutaDiscos from "./rutas/rutaDiscos.js";
import rutaBusquedas from "./rutas/rutaBusquedas.js";
import rutaEliminar from "./rutas/rutaEliminar.js";
import rutaActualizar from "./rutas/rutaActualizar.js";
import rutaConexion from "./rutaConexion.js";

app.set("port", process.env.PORT || 3000);
app.use(morgan("dev"));
app.use(cors());
app.use(express.json());

app.use("/", rutaDiscos);
app.use("/", rutaBusquedas);
app.use("/", rutaEliminar);
app.use("/", rutaActualizar);
app.use("/", rutaConexion);

app.listen(app.get("port"), () => {
  console.log("Servidor en 3000");
});

/**
 * ? Este comando esta en fase experimental
 * * node --watch Servidor/app.js
 */
