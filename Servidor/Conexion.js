import { MongoClient } from "mongodb";
const validacion = {};

//mongodb://Lalo:20680170@127.0.0.1:27017/?authMechanism=DEFAULT&authSource=admin

//mongodb://root:password@127.0.0.1:27017/?authMechanism=DEFAULT&authSource=admin

//mongodb://Lalo:20680170@127.0.0.1:27017/?authMechanism=DEFAULT&authSource=DB_musica

export const mongoURl = {
  nombre: "",
  contraseña: "",
  uri: function () {
    return (
      "mongodb://" +
      this.nombre +
      ":" +
      this.contraseña +
      "@127.0.0.1:27017/?authMechanism=DEFAULT&authSource=DB_musica"
    );
  },
};

// export const mongoURl = {
//   uri: "mongodb://127.0.0.1:27017"
// };

function ingresarDatosLogin(mongoURL, clave, datoLogin) {
  mongoURL[clave] = datoLogin;
}

validacion.validacionDeConexion = async (req, res) => {
  try {
    const nombre = req.body.nombre;
    const contra = req.body.contraseña;

    ingresarDatosLogin(mongoURl, "nombre", nombre);
    ingresarDatosLogin(mongoURl, "contraseña", contra);
    const uri = mongoURl.uri();
    console.log(uri);
    console.log("conectado con: "+nombre);

    const cliente = new MongoClient(uri);
    await cliente.connect();

    console.log('Conexion exitosa');
    res.send("Hubo conexion");
    cliente.close();
  } catch (error) {
    res.status(500).send(error);
    console.error("Hubo error en la conexion");
  }
};

// validacion.validacionDeConexion = async (req, res) => {
//   const nombre = req.body.nombre;
//   const contra = req.body.contraseña;

//   ingresarDatosLogin(mongoURl, "nombre", nombre);
//   ingresarDatosLogin(mongoURl, "contraseña", contra);
//   const uri = mongoURl.uri();

//   const cliente = new MongoClient(uri);

//   cliente.connect((error, client) => {
//     if (error) {
//       res.status(500).send("ERROR");
//       return;
//     }
//     console.log("Conexion Exitosa");
//     res.send("EXITO");
//     client.close();
//   });
// };

export default validacion;

// // const { MongoClient } = require("mongodb");
// import { MongoClient } from "mongodb";

// // Replace the uri string with your connection string.
// const uri = "mongodb://127.0.0.1:27017";

// const client = new MongoClient(uri);

// async function run() {
//   try {
//     const database = client.db("sample_mflix");
//     const movies = database.collection("movies");

//     // const insertResult = await movies.insertMany([
//     //   { a: 1 },
//     //   { a: 2 },
//     //   { a: 3 },
//     // ]);
//     // console.log("Inserted documents =>", insertResult);
//     // Query for a movie that has the title 'Back to the Future'
//     const query = { a: 1 };
//     const movie = await movies.findOne(query);

//     console.log(movie);
//   } finally {
//     // Ensures that the client will close when you finish/error
//     await client.close();
//   }
// }
// run().catch(console.dir);
