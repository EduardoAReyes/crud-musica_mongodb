import express from "express"
import validacion from "./Conexion.js";
const rutaConexion = express.Router();

rutaConexion.post("/conexion",validacion.validacionDeConexion);

export default rutaConexion;