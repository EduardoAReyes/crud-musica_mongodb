import express from "express";
const rutaBusquedas = express.Router();
import controlador from "../Controladores/controladoresBusq.js";

rutaBusquedas.get("/busca_artista", controlador.buscar_artista);
rutaBusquedas.post("/busca_album",controlador.buscar_album);
rutaBusquedas.post("/consultaDatos_album",controlador.consultarDatos_album);
rutaBusquedas.post("/Consulta_Canciones",controlador.consultar_Canciones);

// rutaBusquedas.post("/consulta_albums",controlador.consultar_albums);

export default rutaBusquedas;
