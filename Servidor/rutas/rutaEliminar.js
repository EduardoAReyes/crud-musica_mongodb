import express from "express";
const rutaEliminar = express.Router();
import controlador from "../Controladores/controladorEli.js";

rutaEliminar.post("/EliminaArtista", controlador.eliminar_artista);
rutaEliminar.post("/EliminaAlbum",controlador.eliminar_album);
rutaEliminar.post("/EliminaCancion",controlador.eliminar_cancion);

export default rutaEliminar;
