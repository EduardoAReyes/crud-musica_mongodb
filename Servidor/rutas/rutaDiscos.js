import express from "express";
const rutaDiscos = express.Router();
import controlador from "../Controladores/controladoresDis.js";

rutaDiscos.post("/registra_artista", controlador.registrar_artista);
rutaDiscos.post("/registra_datosAlbum", controlador.registrar_datosAlbum);
rutaDiscos.post("/registra_canciones",controlador.registrar_canciones);

export default rutaDiscos;
