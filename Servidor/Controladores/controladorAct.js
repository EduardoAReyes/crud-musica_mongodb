import { MongoClient } from "mongodb";
import { mongoURl } from "../Conexion.js";
const controlador = {};

controlador.actualizarAlbum = async (req, res) => {
  const cliente = new MongoClient(mongoURl.uri());
  try {
    const nombreArtista = req.body.nombreArtista;
    const caso = req.body.caso;
    const datoAntiguo = req.body.datoAntiguo;
    const nuevoDato = req.body.nuevoDato;

    console.log(nombreArtista);
    console.log(caso);
    console.log(datoAntiguo);
    console.log(nuevoDato);

    const database = cliente.db("DB_musica");
    const artistas = database.collection("Artistas");

    if (caso === "NombreAlbum") {
      const albumActualizado = await artistas.updateOne(
        { NombreArtista: nombreArtista, "Albums.NombreAlbum": datoAntiguo },
        { $set: { "Albums.$.NombreAlbum": nuevoDato } }
      );

      // res.send();
      console.log("Documentos actualizado =>", albumActualizado);
    } else if (caso === "Fecha") {
      const albumActualizado = await artistas.updateOne(
        { NombreArtista: nombreArtista, "Albums.Fecha": datoAntiguo },
        { $set: { "Albums.$.Fecha": nuevoDato } }
      );

      // res.send();
      console.log("Documentos actualizado =>", albumActualizado);
    } else if (caso === "Genero") {
      const albumActualizado = await artistas.updateOne(
        { NombreArtista: nombreArtista, "Albums.Genero": datoAntiguo },
        { $set: { "Albums.$.Genero": nuevoDato } }
      );

      // res.send();
      console.log("Documentos actualizado =>", albumActualizado);
    } else if (caso === "Duración") {
      const albumActualizado = await artistas.updateOne(
        { NombreArtista: nombreArtista, "Albums.Duración": datoAntiguo },
        { $set: { "Albums.$.Duración": nuevoDato } }
      );

      // res.send();
      console.log("Documentos actualizado =>", albumActualizado);
    }

    
    const albumsConsultados = await artistas
      .find({ NombreArtista: nombreArtista, "Albums.NombreAlbum": nombreAlbum })
      .project({ _id: 0, "Albums.$": 1 })
      .toArray();

    const datosAlbumConsultados = albumsConsultados[0].Albums[0];

    res.send(datosAlbumConsultados)

    // const albumActualizado = await artistas.updateOne(
    //   { NombreArtista: nombreArtista, datoAntiguoQuery: datoAntiguo },
    //   { $set: { datoNuevoQuery: nuevoDato } }
    // );

    // res.send();
    // console.log("Documentos actualizado =>", albumActualizado);
  } catch (error) {
    console.error(error);
  } finally {
    await cliente.close();
  }
};

export default controlador;
