import { MongoClient } from "mongodb";
import { mongoURl } from "../Conexion.js";
const controlador = {};

/**
 * ? Comandos usados
 * * actualizar = db.Artistas.update({"NombreArtista":"Nirvana"},{$set:{"Albums.0.NombreAlbum":"Bleach"}})
 */

controlador.registrar_artista = async (req, res) => {
  console.log(mongoURl.uri());
  try {
    const nombreArtista = req.body.nombreArtista;
    const cliente = new MongoClient(mongoURl.uri());

    const database = cliente.db("DB_musica");
    const artistas = database.collection("Artistas");

    const query = { NombreArtista: nombreArtista, Albums: [] };

    const artistaRegistrado = await artistas.insertOne(query);
    // const movie = await movies.findOne(query);
    console.log(nombreArtista);
    console.log("Inserted documents =>", artistaRegistrado);
    res.send();
    await cliente.close();
  } catch (error) {
    res.status(500).send(error);
    console.error(error);
  }
};

controlador.registrar_datosAlbum = async (req, res) => {
  try {
    const nombreArtista = req.body.nombreArtista;
    const nombreAlbum = req.body.nombreAlbum;
    const fecha = req.body.fecha;
    const genero = req.body.genero;
    const duracion = req.body.duracion;

    const cliente = new MongoClient(mongoURl.uri());

    const database = cliente.db("DB_musica");
    const artistas = database.collection("Artistas");

    const datosAlbum_registrado = await artistas.updateOne(
      { NombreArtista: nombreArtista },
      {
        $push: {
          Albums: {
            NombreAlbum: nombreAlbum,
            Fecha: fecha,
            Genero: genero,
            Duración: duracion,
            Canciones: [],
          },
        },
      }
    );

    console.log("Updated documents =>", datosAlbum_registrado);

    const nuevaConsultaAlbum = await artistas.distinct("Albums.NombreAlbum", {
      NombreArtista: nombreArtista,
    });
    res.send(nuevaConsultaAlbum);
    await cliente.close();
  } catch (error) {
    console.error(error);
  }
};

controlador.registrar_canciones = async (req, res) => {
  try {
    const nombreArtista = req.body.nombreArtista;
    const nombreAlbum = req.body.nombreAlbum;
    const cancion = req.body.cancion;

    console.log(nombreArtista);
    console.log(nombreAlbum);
    console.log(cancion);

    const cliente = new MongoClient(mongoURl.uri());

    const database = cliente.db("DB_musica");
    const artistas = database.collection("Artistas");

    /**
     * db.collection.updateOne(
  { _id: ObjectId("12345") },
  { $set: { "array.$[element].field": "new value" } },
  { arrayFilters: [{ "element.field": "value" }] }
);

     */
    const cancionRegistrada = await artistas.updateOne(
      { NombreArtista: nombreArtista },
      { $push: { "Albums.$[album].Canciones": cancion } },
      { arrayFilters: [{ "album.NombreAlbum": nombreAlbum }] }
    );

    console.log("Updated documents =>", cancionRegistrada);
    res.send();
    await cliente.close();
  } catch (error) {
    console.error(error);
  }
};

controlador.registrar = async (req, res) => {
  try {
    const nombre = req.body.nombre;
    const client = new MongoClient(mongoURl.uri());
    const database = client.db("sample_mflix");
    const movies = database.collection("movies");
    const query = { a: 1 };

    const movie = await movies.findOne(query);
    console.log(nombre);
    console.log(movie);
    res.send(movie);

    await client.close();
  } catch (error) {
    res.status(500).send(error);
    console.error(error);
  }
};

export default controlador;
