import { MongoClient } from "mongodb";
import { mongoURl } from "../Conexion.js";
const controlador = {};

controlador.eliminar_artista = async (req, res) => {
  const cliente = new MongoClient(mongoURl.uri());
  try {
    const nombreArtista = req.body.nombreArtista;

    const database = cliente.db("DB_musica");
    const artistas = database.collection("Artistas");

    const query = { NombreArtista: nombreArtista };

    //db.Artistas.distinct("Albums.Canciones",{"NombreArtista":"Nirvana"})
    //db.Artistas.distinct("Albums.Canciones",{"Albums.NombreAlbum":"Bleach"})
    const artistaEliminado = await artistas.deleteOne(query);

    res.send(artistaEliminado);
    console.log("Deleted documents =>", artistaEliminado);
    console.log(nombreArtista);
  } finally {
    await cliente.close();
  }
};

controlador.eliminar_album = async (req, res) => {
  const cliente = new MongoClient(mongoURl.uri());
  try {
    const nombreAlbum = req.body.nombreAlbum;
    const nombreArtista = req.body.nombreArtista;

    const database = cliente.db("DB_musica");
    const artistas = database.collection("Artistas");

    const albumEliminado = await artistas.updateOne(
      { NombreArtista: nombreArtista },
      { $pull: { Albums: { NombreAlbum: nombreAlbum } } }
    );
    console.log("Deleted documents =>", albumEliminado);

    const nuevaConsultaAlbum = await artistas.distinct("Albums.NombreAlbum", {
      NombreArtista: nombreArtista,
    });
    res.send(nuevaConsultaAlbum);
  } finally {
    await cliente.close();
  }
};

controlador.eliminar_cancion = async (req, res) => {
  const cliente = new MongoClient(mongoURl.uri());
  try {
    const nombreArtista = req.body.nombreArtista;
    const nombreAlbum = req.body.nombreAlbum;
    const cancionEliminar = req.body.cancionEliminar;

    console.log(nombreArtista);
    console.log(nombreAlbum);
    console.log(cancionEliminar);

    const database = cliente.db("DB_musica");
    const artistas = database.collection("Artistas");

    const cancionEliminada = await artistas.updateOne(
      {
        NombreArtista: nombreArtista,
        "Albums.NombreAlbum": nombreAlbum,
      },
      { $pull: { "Albums.$.Canciones": cancionEliminar } }
    );
    console.log("Deleted documents =>", cancionEliminada);

    const nuevaConsultaCanciones= await artistas.aggregate([
      { $match: { "NombreArtista": nombreArtista } },
      { $unwind: "$Albums" },
      { $match: { "Albums.NombreAlbum": nombreAlbum } },
      { $project: { _id: 0, Canciones: "$Albums.Canciones" } }
    ]).toArray()

    res.send(nuevaConsultaCanciones);
  } catch (error) {
    console.error(error);
  } finally {
    await cliente.close();
  }
};

export default controlador;
