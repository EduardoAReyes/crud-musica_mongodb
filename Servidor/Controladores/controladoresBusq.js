import { MongoClient } from "mongodb";
import { mongoURl } from "../Conexion.js";
const controlador = {};

controlador.buscar_artista = async (req, res) => {
  const cliente = new MongoClient(mongoURl.uri());
  try {
    const database = cliente.db("DB_musica");
    const artistas = database.collection("Artistas");

    // db.Artistas.findOne({NombreArtista:"Nirvana"},{NombreArtista:1,Albums:1,_id:0})
    // const query = { NombreArtista: "Nirvana" };

    const artistaEncontrado = await artistas.distinct("NombreArtista");
    res.send(artistaEncontrado);
    console.log("Documentos encontrados =>", artistaEncontrado);
    await cliente.close();
  } catch (error) {
    console.error(error);
  }
};

controlador.buscar_album = async (req, res) => {
  const cliente = new MongoClient(mongoURl.uri());
  try {
    const nombreArtista = req.body.nombreArtista;

    const database = cliente.db("DB_musica");
    const artistas = database.collection("Artistas");

    const albumEncontrado = await artistas.distinct("Albums.NombreAlbum", {
      NombreArtista: nombreArtista,
    });

    res.send(albumEncontrado);
    console.log("Documentos encontrados =>", albumEncontrado);
    await cliente.close();
  } catch (error) {
    console.error(error);
  }
};

// controlador.consultar_albums = async (req, res) => {
//   const cliente = new MongoClient(mongoURl.uri());
//   try {
//     const nombreArtista = req.body.NombreArtista;

//     const database = cliente.db("DB_musica");
//     const artistas = database.collection("Artistas");

//     const albumsConsultados = await artistas.distinct("Albums.NombreAlbum", {
//       NombreArtista: nombreArtista,
//     });

//     res.send(albumsConsultados);
//     console.log("Documentos encontrados en consulta=>", albumsConsultados);
//   } finally {
//     await cliente.close();
//   }
// };

controlador.consultarDatos_album = async (req, res) => {
  const cliente = new MongoClient(mongoURl.uri());
  try {
    // const nombreArtista = req.body.NombreArtista;
    const nombreArtista = req.body.nombreArtista;
    const nombreAlbum = req.body.nombreAlbum;

    const database = cliente.db("DB_musica");
    const artistas = database.collection("Artistas");

    //db.Artistas.distinct("Albums", { NombreArtista: "Metallica" });

    // const albumsConsultados = await artistas.findOne({"NombreArtista":"Nirvana"})

    // const project = {"NombreArtista":1}

    // const albumsConsultados = artistas.find().project(project);

    const albumsConsultados = await artistas
      .find({ NombreArtista: nombreArtista, "Albums.NombreAlbum": nombreAlbum })
      .project({ _id: 0, "Albums.$": 1 })
      .toArray();

    const datosAlbumConsultados = albumsConsultados[0].Albums[0];

    console.log("Documentos encontrados =>", datosAlbumConsultados);

    // db.Artistas.find({ "NombreArtista": "Nirvana", "Albums.NombreAlbum": "In Utero" }, { "Albums.$": 1, "_id":0 })

    res.send(datosAlbumConsultados);

    // console.log("Documentos encontrados =>", albumsConsultados);
  } catch (error) {
    res.send(error);
    console.error(error);
  } finally {
    await cliente.close();
  }
};

controlador.consultar_Canciones = async (req, res) => {
  const cliente = new MongoClient(mongoURl.uri());
  try {
    const nombreArtista = req.body.nombreArtista;
    const nombreAlbum = req.body.nombreAlbum;
    const database = cliente.db("DB_musica");
    const artistas = database.collection("Artistas");

    const cancionesConsultadas = await artistas.aggregate([
      { $match: { "NombreArtista": nombreArtista } },
      { $unwind: "$Albums" },
      { $match: { "Albums.NombreAlbum": nombreAlbum } },
      { $project: { _id: 0, Canciones: "$Albums.Canciones" } }
    ]).toArray()

    console.log("Canciones consultadas=>",cancionesConsultadas);
    res.send(cancionesConsultadas);
  } catch (error) {
    console.error(error);
  }
};

export default controlador;
