use("DB_musica");

use("admin");
db.dropUser("Lalo");

use("admin");
db.createUser({
  user: "root",
  pwd: "password",
  roles: ["root"],
});

use("DB_musica");
db.createUser({
  user: "Lalo",
  pwd: "20680170",
  roles: ["read"],
});

use("DB_musica");
db.getUser("Admin", { showCredentials: true });

db.dropUser("Lalo");

use("DB_musica");
db.createUser({
  user: "Lalo",
  pwd: "20680170",
  roles: [""],
});

db.dropAllUsers();

use("DB_musica");
db.getUser("Lalo");

use("DB_musica");
db.createUser({
  user: "Admin",
  pwd: "contra",
  roles: ["read"],
});

use("DB_musica");
db.createRole({
  role: "readOnly",
  privileges: [
    {
      resource: { db: "DB_musica", collection: "Artistas" },
      actions: ["find"],
    },
  ],
  roles: [],
});

db.createRole({});

//db.grantRolesToUser("user3", [{role: "read", db: "sample-database-2"}])

use("DB_musica");
db.grantRolesToUser("Admin", [{ role: "read", db: "DB_musica" }]);

use("DB_musica");
db.grantRolesToUser("Lalo", ["readOnly"]);

use("DB_musica");
db.dropRole("read");

use("DB_musica");
db.createUser({
  user: "Lalo",
  pwd: "20680170",
  roles: [],
});

//
// use("admin");

// db.createUser({
//   user: "root",
//   pwd: "contra",
//   roles: [{ role: "root", db: "admin" }],
// });
