
db.Artistas.find({ Albums: "Nirvana" });

db.Artistas.distinct("Albums.Canciones", { "Albums.NombreAlbum": "Bleach" });


db.Artistas.distinct("Albums", { "NombreArtista": "Nirvana" });



use("DB_musica")
db.Artistas.find().sort({ _id: 1 });


use("DB_musica");
db.Artistas.find().pretty();


db.Artistas.distinct("Albums.Canciones");


db.Artistas.deleteOne({ "Albums.NombreAlbum": "Bleach" });


use("DB_musica");
db.Artistas.updateOne(
  { NombreArtista: "Nirvana" },
  { $pull: { Albums: { NombreAlbum: "Bleach" } } }
);


db.collection.updateOne(
  { _id: ObjectId("646d4cc793807874666c4d02") }, // Filtro para seleccionar el documento específico
  { $pull: { Albums: { NombreAlbum: "Bleach" } } } // Condición para eliminar el álbum
);


db.Artistas.distinct("Albums");


db.Artistas.find().pretty();

db.Artistas.updateOne({"NombreArtista":"Nirvana","NombreAlbum":"Nevermind"},{$set:{"NombreAlbum":"Pipo"}})

/**Este query es para actulizar */
db.Artistas.updateOne(
  { "NombreArtista": "Nirvana", "Albums.NombreAlbum": "Gati" },
  { $set: { "Albums.$.NombreAlbum": "Nevermind" } }
)



db.Artistas.distinct("Albums",{"NombreArtista":"Nirvana","Albums.NombreAlbum":"Bleach"});


db.Artistas.distinct("Albums")



db.Artistas.find({"Albums.NombreAlbum":"Bleach"})

db.Artistas.find({"NombreArtista": "Nirvana", "Albums.NombreAlbum": "Bleach"})





db.Artistas.findOne({"NombreArtista":"Nirvana"}).distinct({"Albums.NombreAlbum":"Bleach"})



db.Artistas.findOne({"Albums.NombreAlbum":"Bleach"})


db.Artistas.distinct("Albums",{"NombreArtista":"Nirvana"})

db.Artistas.find().pretty();

db.Artistas.find({ "NombreArtista": "Nirvana", "Albums.NombreAlbum": "In Utero" }, { "Albums.$": 1, "_id":0 })

db.Artistas.distinct("Albums.Canciones", { "NombreArtista": "Nirvana" });

db.Artistas.findOne(
  { "NombreArtista": "Nirvana", "Albums.NombreAlbum":"Nevermind" },
  { "Albums.$.Canciones": 1 }
)

db.Artistas.aggregate([
  {
    $match: { "NombreArtista": "Nirvana" }
  },
  {
    $unwind: "$Albums"
  },
  {
    $match: { "Albums.NombreAlbum": "Nevermind" }
  },
  {
    $project: {
      _id: 0,
      Canciones: "$Albums.Canciones"
    }
  }
])


db.Artistas.find().pretty()

use("DB_musica")
db.Artistas.deleteOne({NombreArtista:"Shakira"})
